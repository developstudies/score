package br.ufc.quixada.es.persistencia;

import org.hibernate.classic.Session;

public class PreparaSessao {
	
	private static Session sessao = CriarTabelas.prepararSessao();
	
	public static Session retornarSessao() {
		if(sessao == null){
			sessao = CriarTabelas.prepararSessao();
		} else if(!sessao.isOpen()){
			sessao = sessao.getSessionFactory().openSession();
		}
		return sessao;
	}

}

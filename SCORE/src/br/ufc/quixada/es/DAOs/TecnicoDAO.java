package br.ufc.quixada.es.DAOs;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;

import br.ufc.quixada.es.modelos.Tecnico;
import br.ufc.quixada.es.persistencia.PreparaSessao;

public class TecnicoDAO {

	private ReservaDAO daoR = new ReservaDAO();
	private static Session sessao;

	@SuppressWarnings("finally")
	public  boolean insert(Tecnico tecnico) {

		//Cria uma sessao
		boolean inserir = false;
		sessao = PreparaSessao.retornarSessao();

		try{
			//Abre uma transacao
			Transaction trasaction = sessao.beginTransaction();

			//Salva o usuario no banco
			sessao.save(tecnico);

			//Fecha a transacao
			trasaction.commit();
			inserir = true;

		}catch (HibernateException e) {
			System.out.println("Excecao Hibernate: " + e.getMessage() + " :: "
					+ e.toString());
		}
		//Fecha a sessao
		finally{
			sessao.close();
			return inserir;
		}
	}

	@SuppressWarnings("finally")
	public boolean delete(Tecnico tecnico) {

		boolean deletar = false;
		sessao = PreparaSessao.retornarSessao();

		try{
			Transaction trasaction = sessao.beginTransaction();

			Tecnico tecnicoCarregado = (Tecnico) sessao.load(Tecnico.class, tecnico.getId());

			sessao.delete(tecnicoCarregado);

			trasaction.commit();
			deletar = true;
		}catch (ConstraintViolationException e) {
			daoR.deletarTodasReservasTecnico(tecnico);
		}
		finally{
			sessao.close();
			return deletar;
		}



	}

	public  Tecnico select(String codigoAdministrador) {
		sessao = PreparaSessao.retornarSessao();

		Transaction trasaction = sessao.beginTransaction();

		Criteria criteria = sessao.createCriteria(Tecnico.class);
		criteria.add(Restrictions.eq("codigoAdministrador", codigoAdministrador));

		Tecnico tecnico = (Tecnico) criteria.uniqueResult();
		//Tratar erro de usuario
		trasaction.commit();

		sessao.close();

		return tecnico;
	}

	@SuppressWarnings("unchecked")
	public List<Tecnico> selectAll()
	{
		sessao = PreparaSessao.retornarSessao();

		Transaction trasaction = sessao.beginTransaction();

		Criteria criteria = sessao.createCriteria(Tecnico.class);

		List<Tecnico> tecnico = (List<Tecnico>) criteria.list();
		//Tratar erro de usuario
		trasaction.commit();

		sessao.close();

		return tecnico;
	}
	@SuppressWarnings("finally")
	public Tecnico selectlogin(String login, String senha){

		sessao = PreparaSessao.retornarSessao();

		Tecnico tecnico = new Tecnico();
		try{
			String hql = "from Tecnico tecnico where tecnico.login like ? and tecnico.senha like ?";
			Query query = sessao.createQuery(hql).setString(0, login).setString(1, senha);
			tecnico = (Tecnico) query.uniqueResult();
		}
		catch(HibernateException e){
			System.out.println("Excecao Hibernate: " + e.getMessage() + " :: "
					+ e.toString());
		}
		finally{
			sessao.close();
			return tecnico;
		}
	}

	@SuppressWarnings("finally")
	public boolean update(Tecnico novoTecnico) {

		boolean atualizar = false;
		sessao = PreparaSessao.retornarSessao();

		try{
			Transaction trasaction = sessao.beginTransaction();

			Tecnico novoTecnicoCarregado = (Tecnico) sessao.load(Tecnico.class, novoTecnico.getId());

			novoTecnicoCarregado = novoTecnico;

			sessao.update(novoTecnicoCarregado);

			trasaction.commit();
			atualizar = true;
		}catch (HibernateException e) {
			// TODO: handle exception
		}
		finally{
			sessao.close();
			return atualizar;
		}

	}

}


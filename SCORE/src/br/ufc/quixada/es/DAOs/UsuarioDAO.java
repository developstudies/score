package br.ufc.quixada.es.DAOs;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.Query;

import br.ufc.quixada.es.modelos.Usuario;
import br.ufc.quixada.es.persistencia.PreparaSessao;

public class UsuarioDAO {

	private ReservaDAO daoR = new ReservaDAO();
	private static Session sessao;

	@SuppressWarnings("finally")
	public boolean insert(Usuario usuario){

		sessao = PreparaSessao.retornarSessao();
		boolean cadastro = false;

		try{
			Transaction trasaction = sessao.beginTransaction();
			sessao.save(usuario);
			trasaction.commit();
			cadastro = true;
		}
		catch(HibernateException e){
			System.out.println("Excecao Hibernate: " + e.getMessage() + " :: "
					+ e.toString());
		}
		finally{
			sessao.close();
			return cadastro;
		}
	}

	@SuppressWarnings("finally")
	public boolean delete(Usuario usuario){

		sessao = PreparaSessao.retornarSessao();

		boolean excluir = false;

		try{
			Transaction trasaction = sessao.beginTransaction();
			Usuario usuarioCarregado = (Usuario) sessao.load(Usuario.class, usuario.getId());
			sessao.delete(usuarioCarregado);

			trasaction.commit();
			excluir = true;
		}
		catch(ConstraintViolationException e){

			daoR.deletarTodasReservasUsuario(usuario);

		}
		finally{
			sessao.close();
			return excluir;
		}
	}

	@SuppressWarnings("finally")
	public Usuario selectlogin(String login, String senha){

		sessao = PreparaSessao.retornarSessao();

		Usuario usuario = new Usuario();
		try{
			String hql = "from Usuario usuario where usuario.login like ? and usuario.senha like ?";
			Query query = sessao.createQuery(hql).setString(0, login).setString(1, senha);
			usuario = (Usuario) query.uniqueResult();
		}
		catch(HibernateException e){
			System.out.println("Excecao Hibernate: " + e.getMessage() + " :: "
					+ e.toString());
		}
		finally{
			sessao.close();
			return usuario;
		}
	}

	@SuppressWarnings("finally")
	public List<Usuario> selectAll(){

		sessao = PreparaSessao.retornarSessao();

		List<Usuario> usuarios = new ArrayList<Usuario>();
		try{
			Transaction trasaction = sessao.beginTransaction();
			@SuppressWarnings("unchecked")
			ArrayList<Usuario> list = (ArrayList<Usuario>)sessao.createCriteria(Usuario.class).list();
			usuarios = list;
			trasaction.commit();
		}
		catch(HibernateException e){
			System.out.println("Excecao Hibernate: " + e.getMessage() + " :: "
					+ e.toString());
		}
		finally{
			sessao.close();
			return usuarios;
		}
	}


	@SuppressWarnings("finally")
	public boolean update(Usuario usuario){
		sessao = PreparaSessao.retornarSessao();

		boolean atualizar = false;

		try{
			Transaction trasaction = sessao.beginTransaction();

			Usuario novoUsuarioCarregado = (Usuario) sessao.load(Usuario.class, usuario.getId());
			novoUsuarioCarregado = usuario;

			sessao.update(novoUsuarioCarregado);
			trasaction.commit();

			atualizar = true;
		}
		catch(HibernateException e){
			System.out.println("Excecao Hibernate: " + e.getMessage() + " :: "
					+ e.toString());
		}
		finally{
			sessao.close();
			return atualizar;
		}
	}
}

package br.ufc.quixada.es.DAOs;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;

import br.ufc.quixada.es.modelos.Recurso;
import br.ufc.quixada.es.modelos.Reserva;
import br.ufc.quixada.es.modelos.Tecnico;
import br.ufc.quixada.es.modelos.Usuario;

import br.ufc.quixada.es.persistencia.PreparaSessao;

public class ReservaDAO {

	private static Session sessao;

	@SuppressWarnings("unchecked")
	public ArrayList<Reserva> listarTodas(){

		sessao = PreparaSessao.retornarSessao();

		Transaction trasaction = sessao.beginTransaction();

		Criteria criteria = sessao.createCriteria(Reserva.class);
		ArrayList<Reserva> reservas =  (ArrayList<Reserva>) criteria.list();

		trasaction.commit();
		sessao.close();

		return reservas;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Reserva> listarReservasAprovadas(){

		sessao = PreparaSessao.retornarSessao();

		Transaction transaction = sessao.beginTransaction();

		Criteria criteria = sessao.createCriteria(Reserva.class);
		criteria.add(Restrictions.eq("status", "Aprovada"));

		ArrayList<Reserva> reservas = (ArrayList<Reserva>) criteria.list();

		transaction.commit();
		sessao.close();

		return reservas;
	}



	@SuppressWarnings("unchecked")
	public ArrayList<Reserva> listarReservasNaoAprovadas() {
		sessao = PreparaSessao.retornarSessao();

		Transaction transaction = sessao.beginTransaction();

		Criteria criteria = sessao.createCriteria(Reserva.class);
		criteria.add(Restrictions.eq("status", "Nao Aprovada"));

		ArrayList<Reserva> reservas = (ArrayList<Reserva>) criteria.list();

		transaction.commit();
		sessao.close();

		return reservas;

	}

	@SuppressWarnings("finally")
	public boolean deletar(Reserva reserva){

		boolean deletar = false;

		sessao = PreparaSessao.retornarSessao();

		try{
			Transaction trasaction = sessao.beginTransaction();
			Reserva reservaCarregada = (Reserva) sessao.load(Reserva.class, reserva.getId());

			sessao.delete(reservaCarregada);

			trasaction.commit();
			deletar = true;
		}
		catch (HibernateException e) {
			// TODO: handle exception
		}
		finally{
			sessao.close();
			return deletar;
		}

	}

	@SuppressWarnings("finally")
	public boolean deletarTodasReservasUsuario(Usuario usuario){

		boolean deletar = false;

		sessao = PreparaSessao.retornarSessao();

		try{
			Transaction trasaction = sessao.beginTransaction();
			List<Reserva> reservasUsuario = buscarReservaPorUsuario(usuario);

			for (Reserva reserva : reservasUsuario) {
				sessao.delete(reserva);
			}

			trasaction.commit();
			deletar = true;
		}
		catch (HibernateException e) {
			// TODO: handle exception
		}
		finally{
			sessao.close();
			return deletar;
		}

	}

	@SuppressWarnings("finally")
	public boolean deletarTodasReservasRecurso(Recurso recurso){

		boolean deletar = false;

		sessao = PreparaSessao.retornarSessao();

		try{
			Transaction trasaction = sessao.beginTransaction();
			List<Reserva> reservasUsuario = buscarReservaPorRecurso(recurso);

			for (Reserva reserva : reservasUsuario) {
				sessao.delete(reserva);
			}

			trasaction.commit();
			deletar = true;
		}
		catch (HibernateException e) {
			// TODO: handle exception
		}
		finally{
			sessao.close();
			return deletar;
		}

	}

	@SuppressWarnings("finally")
	public boolean cadastrar(Reserva reserva){

		sessao = PreparaSessao.retornarSessao();
		boolean cadastro = false;
		try{
			Transaction trasaction = sessao.beginTransaction();
			reserva.setStatus("Nao Aprovada");
			sessao.save(reserva);

			trasaction.commit();
			cadastro = true;

		}catch (HibernateException e) {
			System.out.println("Excecao Hibernate: " + e.getMessage() + " :: "
					+ e.toString());
		}
		finally{
			sessao.close();
			return cadastro;
		}
	}

	@SuppressWarnings("finally")
	public boolean atualizar(Reserva novaReserva){

		boolean atualizar = false;
		sessao = PreparaSessao.retornarSessao();

		try{

			Transaction trasaction = sessao.beginTransaction();

			Reserva reservaCarregada = (Reserva) sessao.load(Reserva.class, novaReserva.getId());

			reservaCarregada = novaReserva;

			sessao.update(reservaCarregada);
			atualizar = true;

			trasaction.commit();
		}catch (HibernateException e) {
			System.out.println("Excecao Hibernate: " + e.getMessage() + " :: "
					+ e.toString());
		}
		finally{
			sessao.close();
			return atualizar;
		}
	}

	public Reserva buscarReservaPor(Long id){

		sessao = PreparaSessao.retornarSessao();
		Transaction trasaction = sessao.beginTransaction();

		Criteria criteria = sessao.createCriteria(Reserva.class);
		criteria.add(Restrictions.eq("id", id));

		Reserva reserva =  (Reserva) criteria.uniqueResult();

		trasaction.commit();
		sessao.close();

		return reserva;
	}

	@SuppressWarnings("unchecked")
	public List<Reserva> buscarReservaPorUsuario(Usuario u){
		sessao = PreparaSessao.retornarSessao();

		Transaction trasaction = sessao.beginTransaction();

		Criteria criteria = sessao.createCriteria(Reserva.class);
		criteria.add(Restrictions.eq("usuario", u));

		List<Reserva> reservas = criteria.list();

		trasaction.commit();
		sessao.close();

		return reservas;
	}

	@SuppressWarnings("unchecked")
	public List<Reserva> buscarReservaPorRecurso(Recurso recurso){
		sessao = PreparaSessao.retornarSessao();

		Transaction trasaction = sessao.beginTransaction();

		Criteria criteria = sessao.createCriteria(Reserva.class);
		criteria.add(Restrictions.eq("recurso", recurso));

		List<Reserva> reservas = criteria.list();

		trasaction.commit();
		sessao.close();

		return reservas;
	}

	@SuppressWarnings("unchecked")
	public List<Reserva> buscarReservaPorTecnico(Tecnico tecnico){
		sessao = PreparaSessao.retornarSessao();

		Transaction trasaction = sessao.beginTransaction();

		Criteria criteria = sessao.createCriteria(Reserva.class);
		criteria.add(Restrictions.eq("tecnico", tecnico));

		List<Reserva> reservas = criteria.list();

		trasaction.commit();
		sessao.close();

		return reservas;
	}

	//Criei
	@SuppressWarnings("finally")
	public boolean aprovar(Reserva reserva, Tecnico tecnico) {

		boolean aprovar = false;
		sessao = PreparaSessao.retornarSessao();

		try{
			Transaction trasaction = sessao.beginTransaction();

			Reserva reservaCarregada = (Reserva) sessao.load(Reserva.class, reserva.getId());

			reservaCarregada = reserva;
			reservaCarregada.setStatus("Aprovada");
			reservaCarregada.setTecnico(tecnico);

			sessao.update(reservaCarregada);
			aprovar = true;

			trasaction.commit();
		}catch (HibernateException e) {

		}
		finally{
			sessao.close();
			return aprovar;
		}

	}

	public void associarUsuarioAReserva(Long idReserva, Usuario usuario) {

		sessao = PreparaSessao.retornarSessao();
		Transaction trasaction = sessao.beginTransaction();

		Reserva reservaCarregada = (Reserva) sessao.load(Reserva.class, idReserva);

		reservaCarregada.setUsuario(usuario);

		sessao.update(reservaCarregada);

		trasaction.commit();
		sessao.close();

	}

	@SuppressWarnings("finally")
	public boolean deletarTodasReservasTecnico(Tecnico tecnico) {

		boolean deletar = false;

		sessao = PreparaSessao.retornarSessao();

		try{
			Transaction trasaction = sessao.beginTransaction();
			List<Reserva> reservasTecnico = buscarReservaPorTecnico(tecnico);

			for (Reserva reserva : reservasTecnico) {
				sessao.delete(reserva);
			}

			trasaction.commit();
			deletar = true;
		}
		catch (HibernateException e) {
			// TODO: handle exception
		}
		finally{
			sessao.close();
			return deletar;
		}

	}

}

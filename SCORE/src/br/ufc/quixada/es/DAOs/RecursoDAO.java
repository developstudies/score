package br.ufc.quixada.es.DAOs;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;

import br.ufc.quixada.es.modelos.Recurso;

import br.ufc.quixada.es.persistencia.PreparaSessao;

public class RecursoDAO {

	private static Session sessao;
	private ReservaDAO daoR = new ReservaDAO();

	@SuppressWarnings("finally")
	public boolean insert(Recurso recurso){
		boolean inserir = false;

		sessao = PreparaSessao.retornarSessao();

		try{
			Transaction trasaction = sessao.beginTransaction();
			sessao.save(recurso);
			trasaction.commit();
			inserir = true;

		} catch (HibernateException e) {
			System.out.println("Excecao Hibernate: " + e.getMessage() + " :: "
					+ e.toString());
		}
		finally{
			sessao.close();
			return inserir;
		}
	}

	@SuppressWarnings("finally")
	public boolean delete(Recurso recurso) {

		boolean deletar = false;

		sessao = PreparaSessao.retornarSessao();

		try{
			Transaction trasaction = sessao.beginTransaction();

			Recurso recursoCarregado = (Recurso) sessao.load(Recurso.class, recurso.getIdRecurso());

			sessao.delete(recursoCarregado);

			trasaction.commit();
			deletar = true;
		}catch (ConstraintViolationException e) {
			daoR.deletarTodasReservasRecurso(recurso);
		}
		finally{

			sessao.close();
			return deletar;
		}

	}

	@SuppressWarnings("finally")
	public boolean update(Recurso novoRecurso) {

		boolean atualizar = false;

		sessao = PreparaSessao.retornarSessao();

		try{Transaction trasaction = sessao.beginTransaction();

		Recurso novoRecursoCarregado = (Recurso) sessao.load(Recurso.class, novoRecurso.getIdRecurso());

		novoRecursoCarregado = novoRecurso;

		sessao.update(novoRecursoCarregado);

		trasaction.commit();
		atualizar = true;

		}catch (HibernateException e) {

		}
		finally{
			sessao.close();
			return atualizar;
		}

	}

	public Recurso selectUnicoRecurso(int numeroRecurso){

		sessao = PreparaSessao.retornarSessao();

		Transaction trasaction = sessao.beginTransaction();

		Criteria criteria = sessao.createCriteria(Recurso.class);
		criteria.add(Restrictions.eq("numeroRecurso", numeroRecurso));

		Recurso recurso = (Recurso) criteria.uniqueResult();

		trasaction.commit();

		sessao.close();

		return recurso;
	}

	@SuppressWarnings("unchecked")
	public List<Recurso> select() {

		sessao = PreparaSessao.retornarSessao();

		Transaction trasaction = sessao.beginTransaction();

		Criteria criteria = sessao.createCriteria(Recurso.class);
		ArrayList<Recurso> recursos = (ArrayList<Recurso>) criteria.list();

		trasaction.commit();

		sessao.close();

		return recursos;
	}

	@SuppressWarnings("unchecked")
	public List<Recurso> selectDisponibilidade(){

		sessao = PreparaSessao.retornarSessao();

		Transaction trasaction = sessao.beginTransaction();

		Criteria criteria = sessao.createCriteria(Recurso.class);
		criteria.add(Restrictions.eq("disponibilidade", false));

		ArrayList<Recurso> recursos = (ArrayList<Recurso>) criteria.list();

		trasaction.commit();

		sessao.close();

		return recursos; 
	}

	public void modificarDisponibilidade(Recurso recurso){

		sessao = PreparaSessao.retornarSessao();

		Transaction trasaction = sessao.beginTransaction();

		recurso.setDisponibilidade(false);
		Recurso novoRecursoCarregado = (Recurso) sessao.load(Recurso.class, recurso.getIdRecurso());

		novoRecursoCarregado = recurso;

		sessao.update(novoRecursoCarregado);

		trasaction.commit();
		sessao.close();

	}	
}
